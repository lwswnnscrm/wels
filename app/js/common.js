$(document).ready(function(){
  show_story();
  slidedown();

  $('#nav-icon1').on('click', function (e) {
      e.preventDefault();
      $(this).toggleClass('is-active');
      $('.dropmenu').toggleClass("active");
  });

  $('#nav-icon1').click(function () {
      console.log("click");
      $(this).toggleClass('open');
  });


  var el_lenght = 0;
  if ($(window).width() < 1200 && $(window).width() >= 768) {
      el_lenght = 4;
      slidedown2(el_lenght);
  }
  else if ($(window).width() < 768) {
      el_lenght = 3;
      slidedown2(el_lenght);
  }
  else if ($(window).width() >= 1200) {
      el_lenght = $(".experinblock .expertsintext").length;
      slidedown2(el_lenght);
  }
  $(window).resize(function () {
      console.log('resize');
      if ($(window).width() < 1200 && $(window).width() > 768) {
          el_lenght = 4;
          slidedown2(el_lenght);
      }
      else if ($(window).width() <= 768) {
          el_lenght = 3;
          slidedown2(el_lenght);
      }
      else if ($(window).width() >= 1200) {
          el_lenght = $(".experinblock .expertsintext").length;
          slidedown2(el_lenght);
      }
  })
})


function show_story() {
  $('.left_story a').click(function(e) {
    e.preventDefault()
    var el = $(this);
    var index = $(this).closest('li').index();
    var el_li = $(this).closest('li')
    console.log(index);
    console.log(el_li)
    $('.right_story .text_content').not(el_li).hide();
    $('.right_story .text_content').eq(index).slideDown();
    $('.left_story li').removeClass('active')
    $(el).closest('li').addClass('active')
  })
}


function slidedown() {
    $('#pulldown').click(function (e) {
        e.preventDefault();
        var openBclick = $(this);
        var textblock = $('.textanimblock');
        var fullHeight = "auto";
        var startHeight = 178;
        var scroll_now = $(window).scrollTop();
        $(openBclick).toggleClass('open');
        if ($(openBclick).hasClass('open')) {
            $(textblock).css('height', fullHeight);
            var add_scroll = parseInt($(textblock).css('height')) - startHeight;
            $('body,html').animate({scrollTop: scroll_now + add_scroll}, 1000);
        }
        else {
            $(textblock).css('height', startHeight + "px")
        }
    })
}

function slidedown2(el_lenght) {
    $(".experinblock .expertsintext").hide()
    console.log(el_lenght)
    var comment_size = $(".experinblock .expertsintext").length;
    var x = el_lenght;
    $('.experinblock .expertsintext:lt(' + x + ')').show();
    $('#clicktoview').click(function (event) {
        event.preventDefault();
        x = (x + el_lenght <= comment_size) ? x + el_lenght : comment_size;
        $('.experinblock .expertsintext:lt(' + x + ')').show();
    });
}
